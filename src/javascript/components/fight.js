import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise(
    (resolve) => {
      // resolve the promise with the winner when fight is over

      let playerOneBlockEnable = false; //
      let playerTwoBlockEnable = false;

      let currentOne = firstFighter.health; // current HP of fighters
      let currentTwo = secondFighter.health;

      const healthbars = document.querySelectorAll('.arena___health-bar');

      window.addEventListener('keyup', (e) => {
        if (playerOneBlockEnable || playerTwoBlockEnable) {
        } else {
          if (e.code === controls.PlayerOneAttack && e.type === 'keyup') {
            const hit = getDamage(firstFighter, secondFighter);
            if (currentTwo > hit) {
              currentTwo -= hit;
              const healthbarUpdate = (currentTwo / secondFighter.health) * 100;
              healthbars[1].style.width = `${healthbarUpdate}%`;
            } else {
              window.removeEventListener('keyup', e);
              resolve(firstFighter);
            }
          }
          if (e.code === controls.PlayerTwoAttack && e.type === 'keyup') {
            const hit = getDamage(secondFighter, firstFighter);
            if (currentOne > hit) {
              currentOne -= hit;
              const healthbarUpdate = (currentOne / firstFighter.health) * 100;
              healthbars[0].style.width = `${healthbarUpdate}%`;
            } else {
              window.removeEventListener('keyup', e);
              resolve(secondFighter);
            }
          }
        }

        if (e.code === controls.PlayerOneBlock) {
          playerOneBlockEnable = false;
        }
        if (e.code === controls.PlayerTwoBlock) {
          playerTwoBlockEnable = false;
        }
      });

      window.addEventListener('keydown', (e) => {
        if (e.code === controls.PlayerOneBlock) {
          playerOneBlockEnable = true;
        }
        if (e.code === controls.PlayerTwoBlock) {
          playerTwoBlockEnable = true;
        }
      });
    },
    firstFighter,
    secondFighter
  );
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(0, damage);
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  const powerHit = fighter.attack * criticalHitChance;
  return powerHit;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() + 1;
  const powerDef = fighter.defense * dodgeChance;
  return powerDef;
}
