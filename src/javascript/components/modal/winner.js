import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  // call showModal function (+)
  const title = `And winner is : ${fighter.name} `;

  const attributes = {
    src: fighter.source,
    alt: fighter.name,
  };
  const fighterImage = createElement({
    tagName: 'img',
    className: 'winner-preview',
    attributes,
  });

  const bodyElement = fighterImage;

  showModal({
    title,
    bodyElement,
    onClose: () => {
      location.reload();
    },
  });
}
