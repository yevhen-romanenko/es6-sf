import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.) (+)

  if (fighter) {   

    const figtherName = createElement({
      tagName: 'span',
      className: 'fighter-name',
    });

    figtherName.innerText = fighter.name;

    const fighterHealth = createElement({
      tagName: 'span',
      className: 'fighter-health',
    });

    fighterHealth.innerText = `HP: ${fighter.health}`;

    const fighterAttack = createElement({
      tagName: 'span',
      className: 'fighter-attack',
    });

    fighterAttack.innerText = `ATK: ${fighter.attack}`;

    const fighterDefense = createElement({
      tagName: 'span',
      className: 'fighter-defense',
    });

    fighterDefense.innerText = `DEF: ${fighter.defense}`;

    const attributes = {
      src: fighter.source,
      alt: fighter.name,
    };
    const fighterImage = createElement({
      tagName: 'img',
      className: 'fighter-preview___img',
      attributes,
    }); 

    fighterElement.append(fighterImage, figtherName, fighterHealth, fighterAttack, fighterDefense);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
